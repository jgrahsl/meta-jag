# Distro features
# Image features are 
# Extra Install


SUMMARY = "A small jag image just capable of allowing a device to boot."
DESCRIPTION = "Jag Test Image"
LICENSE = "MIT"

IMAGE_FEATURES:append = " ssh-server-openssh tools-sdk"

IMAGE_INSTALL = " \
    packagegroup-core-boot \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    v4l-utils \
    gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0 \
    git tmux less htop systemd-analyze vim bash-completion  glibc-utils localedef \
    docker-ce nftables libnfnetlink iptables \
    vim \
    python3-docker-compose \
    kernel-modules \
    tegra-nvpmodel tegra-brcm-patchram tegra-firmware nvidia-docker \
    gstreamer1.0-plugins-tegra gstreamer1.0-omx-tegra gstreamer1.0-plugins-nveglgles gstreamer1.0-plugins-nvvideo4linux2 gstreamer1.0-plugins-nvvideosinks gstreamer1.0-plugins-nvjpeg  \
    vsftpd curl \
    cmake \
    libgpiod spitools \
    dtc \
"

#    cuda-toolkit cuda-command-line-tools cuda-cudart cuda-driver 
#    tensorrt libvisionworks libvisionworks-sfm libvisionworks-tracking tensorrt 
#    deepstream-5.0 
 
inherit core-image
